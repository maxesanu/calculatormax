/**
* This is a very basic arithmetical calculator
* <p>
* This super awesome class performs an arithmetical operation read
* from the keyboard on two numbers that are also read from the keyboard
* for multiplication use 'x' instead of * because your command line does not accept it
* No input validation is performed on numbers. Use at your own risk :)
* <p>
* If you perform division, zero will not be accepted as a third parameter
* Error will be displayed
* @see Math
*
*/

public class Calculator{
	public static void main(String[] args){
		double a = Double.parseDouble(args[1]);
		double b = Double.parseDouble(args[2]);
		double c=0;
		
		switch (args[0]){
			case "+" : c = a+b; System.out.println(a+" + "+b+" = "+c);
			break;
			case "-" : c = a-b; System.out.println(a+" - "+b+" = "+c);
			break;
			case "x" : c = a*b; System.out.println(a+" * "+b+" = "+c);
			break;
			case "/" : if(b==0) {
							System.out.println("For this operation 3rd argument cannot be zero");
						}			
						else {
							c = a/b;
							System.out.println(a+" / "+b+" = "+c);
						};
			break;
			default : System.out.println("Invalid operand!");
			
		};
			
	}	
}
